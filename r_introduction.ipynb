{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction to R and RStudio\n",
    "\n",
    "A general introduction to R and RStudio. Includes installation, core concepts, and best practices."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Set up\n",
    "\n",
    "The latest version of R can be downloaded and installed from [here](https://www.r-project.org/) and RStudio from [here](https://www.rstudio.com/). If you already have them installed on your computer make sure that they are up to date. It's important to have the latest versions to ensure that the packages that we will use can be installed correctly.\n",
    "\n",
    "## RStudio\n",
    "\n",
    "The first time you open RStudio, you will see three windows: the R console on the left, the Environment/History on the upper right, and the Files/Plots/Packages/Help/Viewer on the lower right.\n",
    "\n",
    "## Prompts\n",
    "\n",
    "Everytime you start R, the console shows some information about R. Below, the R console shows a `>` prompt that means R is ready to accept commands. If you type a command R will try to execute it, after it is ready, it will show the results and a new `>` prompt in the next line to wait for new commands.\n",
    "\n",
    "If the console shows a `+` prompt it means that R is still waiting for you to enter more data because you haven't finished entering a complete command. This happens when you have not entered a closing a parenthesis or quotation symbol. To cancel that command, rrom RStudio you can click inside the console and press the `Esc` key. \n",
    "\n",
    "## Workflow\n",
    "\n",
    "You can interact directly with the console, but it is considered best practice to write an .R file and run commands in the console from that script. This allows saving the code and running all at once with the `source` function.\n",
    "\n",
    "Once you open an R script (or any file), an editor panel will appear in the top left. From there you can run the current line (or a selected block of code) by clicking on the `Run` button in the editor panel, or in the `Code` menu select `Run Lines`. There is also a `Re-run` button next to the `Run` button, which allows to run again the previous code block including any new modifications you have just made.\n",
    "\n",
    "### Shortcuts\n",
    "\n",
    "A practical way of running code from a script by using a keyboard shortcut. In GNU/Linux and Windows, you can hit `Ctrl`+`Return`, on macOS the equivalent is `⌘ Command`+`Return`. This shortcut also allows running a selected block of code. \n",
    "\n",
    "You can alternate between the script and console panels with the `Ctrl`+`1` and `Ctrl`+`2` shortcuts.\n",
    "\n",
    "From the console, you can go back to the previous command by hitting the `up` arrow. If you press the `up` arrow multiple times you will walk back through the history. You can navigate the history by pressing the `down` arrow as well. You can edit any command from there by using the `left` arrow or `delete` keys.\n",
    "\n",
    "## Comments\n",
    "\n",
    "In R, the `#` sign is a special character that indicates a comment. Anything that is written directly to the right of a `#` sign will be ignored by R."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Using R as a calculator\n",
    "\n",
    "The simplest thing you can do with R is arithmetics:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "19"
      ],
      "text/latex": [
       "19"
      ],
      "text/markdown": [
       "19"
      ],
      "text/plain": [
       "[1] 19"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "1 + 7 + 2 + 9"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "1729"
      ],
      "text/latex": [
       "1729"
      ],
      "text/markdown": [
       "1729"
      ],
      "text/plain": [
       "[1] 1729"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "9^3 + 10^3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Precedence\n",
    "\n",
    "The order of operations follows the standard precedence (from highest to lowest):\n",
    "\n",
    "* Parentheses `(`, `)`\n",
    "* Exponents or roots: `^`\n",
    "* Multiplication or division: `*`, `/`\n",
    "* Addition or subtraction: `+`, `-`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "31"
      ],
      "text/latex": [
       "31"
      ],
      "text/markdown": [
       "31"
      ],
      "text/plain": [
       "[1] 31"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "1 + (2 * 3 * 5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "45"
      ],
      "text/latex": [
       "45"
      ],
      "text/markdown": [
       "45"
      ],
      "text/plain": [
       "[1] 45"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "(1 + 2) * 3 * 5 "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Mathematical functions\n",
    "\n",
    "R comes with many built in mathematical functions. Functions are called by typing its name followed by open and closing parentheses. Whatever we write inside the parentheses are the function's arguments:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "2.19722457733622"
      ],
      "text/latex": [
       "2.19722457733622"
      ],
      "text/markdown": [
       "2.19722457733622"
      ],
      "text/plain": [
       "[1] 2.197225"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "log(9)  # natural logarithm"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Variables and assignment\n",
    "\n",
    "The assignment operator is `<-`. It stores whatever you write on the right to variables on\n",
    "the left. \n",
    "\n",
    "Variable names can be made up of letters, numbers, underscores and periods. But, they cannot start with a number nor contain spaces. \n",
    "\n",
    "### Shortcuts\n",
    "\n",
    "In RStudio, the assignment operator is written in a keystroke by hitting `Alt`+`-`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "x <- 7"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "7"
      ],
      "text/latex": [
       "7"
      ],
      "text/markdown": [
       "7"
      ],
      "text/plain": [
       "[1] 7"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "# Assigning a variable does not print its value, \n",
    "# but if you look at the `Environment` panel, you will see the new variable there.\n",
    "# You can also check that it is stored by calling the variable\n",
    "x"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "2.64575131106459"
      ],
      "text/latex": [
       "2.64575131106459"
      ],
      "text/markdown": [
       "2.64575131106459"
      ],
      "text/plain": [
       "[1] 2.645751"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "# Variables can be used in calculations\n",
    "sqrt(x)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "314"
      ],
      "text/latex": [
       "314"
      ],
      "text/markdown": [
       "314"
      ],
      "text/plain": [
       "[1] 314"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "# Variables can also be reassigned\n",
    "\n",
    "x <- 314\n",
    "x"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Vectorization\n",
    "\n",
    "So far we assigned our variables with **scalars** (a 1 dimensional object with a length of 1). But, R is a vectorized language, which means that variables and functions can have vectors as values. A **vector** is a set of values, in a specific order, and of the same data type. \n",
    "\n",
    "Vectors are **atomic**, which means that they can only contain one class of data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<ol class=list-inline>\n",
       "\t<li>'red'</li>\n",
       "\t<li>'green'</li>\n",
       "\t<li>'blue'</li>\n",
       "</ol>\n"
      ],
      "text/latex": [
       "\\begin{enumerate*}\n",
       "\\item 'red'\n",
       "\\item 'green'\n",
       "\\item 'blue'\n",
       "\\end{enumerate*}\n"
      ],
      "text/markdown": [
       "1. 'red'\n",
       "2. 'green'\n",
       "3. 'blue'\n",
       "\n",
       "\n"
      ],
      "text/plain": [
       "[1] \"red\"   \"green\" \"blue\" "
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/html": [
       "<ol class=list-inline>\n",
       "\t<li>3</li>\n",
       "\t<li>4</li>\n",
       "\t<li>5</li>\n",
       "\t<li>6</li>\n",
       "\t<li>7</li>\n",
       "</ol>\n"
      ],
      "text/latex": [
       "\\begin{enumerate*}\n",
       "\\item 3\n",
       "\\item 4\n",
       "\\item 5\n",
       "\\item 6\n",
       "\\item 7\n",
       "\\end{enumerate*}\n"
      ],
      "text/markdown": [
       "1. 3\n",
       "2. 4\n",
       "3. 5\n",
       "4. 6\n",
       "5. 7\n",
       "\n",
       "\n"
      ],
      "text/plain": [
       "[1] 3 4 5 6 7"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "rgb <- c(\"red\", \"green\", \"blue\")\n",
    "rgb\n",
    "y <- 3:7\n",
    "y"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "'numeric'"
      ],
      "text/latex": [
       "'numeric'"
      ],
      "text/markdown": [
       "'numeric'"
      ],
      "text/plain": [
       "[1] \"numeric\""
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/html": [
       "'character'"
      ],
      "text/latex": [
       "'character'"
      ],
      "text/markdown": [
       "'character'"
      ],
      "text/plain": [
       "[1] \"character\""
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "# check the type of data\n",
    "class(x)\n",
    "class(rgb)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "5"
      ],
      "text/latex": [
       "5"
      ],
      "text/markdown": [
       "5"
      ],
      "text/plain": [
       "[1] 5"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "# Vectors can also be used as arguments to functions\n",
    "length(y) # count the number of elements in y"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "'character'"
      ],
      "text/latex": [
       "'character'"
      ],
      "text/markdown": [
       "'character'"
      ],
      "text/plain": [
       "[1] \"character\""
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "# But different classes cannot be mixed in the same object\n",
    "semiprimes <- c(4, 6, 9, \"ten\")\n",
    "class(semiprimes)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "Warning message in mean.default(semiprimes):\n",
      "“argument is not numeric or logical: returning NA”"
     ]
    },
    {
     "data": {
      "text/html": [
       "&lt;NA&gt;"
      ],
      "text/latex": [
       "<NA>"
      ],
      "text/markdown": [
       "&lt;NA&gt;"
      ],
      "text/plain": [
       "[1] NA"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "# If you do, you cannot do perform operations with the object\n",
    "mean(semiprimes)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<ol class=list-inline>\n",
       "\t<li>9</li>\n",
       "\t<li>12</li>\n",
       "\t<li>15</li>\n",
       "\t<li>18</li>\n",
       "\t<li>21</li>\n",
       "</ol>\n"
      ],
      "text/latex": [
       "\\begin{enumerate*}\n",
       "\\item 9\n",
       "\\item 12\n",
       "\\item 15\n",
       "\\item 18\n",
       "\\item 21\n",
       "\\end{enumerate*}\n"
      ],
      "text/markdown": [
       "1. 9\n",
       "2. 12\n",
       "3. 15\n",
       "4. 18\n",
       "5. 21\n",
       "\n",
       "\n"
      ],
      "text/plain": [
       "[1]  9 12 15 18 21"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "# The core idea behind vectorization is that operations can be applied at once \n",
    "# to an entire set of values\n",
    "y * 3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Help!\n",
    "\n",
    "You can ask R for help with commands and functions. The general syntax for functions is `?function_name` or `help(function_name)`. There is also a fuzzy search that allows searching keywords `??function_name`. For help with operators you need to use quotes `?\"<-\"`.\n",
    "\n",
    "Typing any of the above will open the help page of the command/function/operator. At the bottom of the help page there will usually be an example to illustrate its usage.\n",
    "\n",
    "### Cheatsheets\n",
    "\n",
    "From RStudio, you can access cheatsheets from the `Help` button on the left of the main menu."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Excercise\n",
    "\n",
    "Create a vector with a sequence of numbers using the function `seq`. Hint: try `?seq`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## R Packages\n",
    "\n",
    "Functions can be added to R with packages, which are stored on [CRAN](https://cran.r-project.org/web/packages/index.html) (the Comprehensive R Archive Network). \n",
    "\n",
    "To install a package you use the function `install.packages(\"packagename\")`, where `packagename` is the name of the package you want to install. To see what packages are installed by type `installed.packages()`.\n",
    "\n",
    "Packages are loaded with the `library(\"packagename\")` function. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercise\n",
    "\n",
    "Install and load the packages ggplot2 and plyr. Don't forget the quotation marks when installing! "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## References\n",
    "\n",
    "1. http://swcarpentry.github.io/r-novice-gapminder/\n",
    "2. https://swcarpentry.github.io/r-novice-inflammation/"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "R",
   "language": "R",
   "name": "ir"
  },
  "language_info": {
   "codemirror_mode": "r",
   "file_extension": ".r",
   "mimetype": "text/x-r-source",
   "name": "R",
   "pygments_lexer": "r",
   "version": "3.4.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
