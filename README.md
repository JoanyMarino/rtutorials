# RTutorials

Tutorials for R.


## In this collection

* Introduction to R and RStudio [r_introduction.ipynb](r_introduction.ipynb)
* Diversity analysis with `vegan` [vegan_diversity.ipynb](vegan_diversity.ipynb)


## License

This project is licensed under the GPL v3.0 License - 
see the [COPYING.md](COPYING.md) file for details.
